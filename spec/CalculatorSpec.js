var Calculator = require('../calculator.js');

describe('Calculator', () => {
	let calculator;

	beforeAll(() => {
		calculator = new Calculator();
	});
	beforeEach(() => {
		calculator.clear();
	});

	it('Calculator initializes with outcome 0 and calculation \'\'', () => {
		expect(calculator.outcome).toBe(0);
		expect(calculator.calculation).toBe('');
	});

	describe('Basic symbol and number buttons', () => {
		it('Number presses add to outcome', () => {
			calculator.one();
			calculator.two();
			calculator.three();
			calculator.four();
			calculator.five();
			calculator.six();
			calculator.seven();
			calculator.eight();
			calculator.nine();
			calculator.zero();

			expect(calculator.outcome).toBe(1234567890);
		});

		it('Replace symbols correctly', () => {
			calculator.nine();
			calculator.plus();
			calculator.minus();
			calculator.five();
			calculator.plus();
			calculator.minus();
			calculator.four();
			expect(calculator.outcome).toBe(4);
			expect(calculator.calculation).toBe('9 ' + calculator.minusSymbol + ' 5 ' + calculator.minusSymbol + ' ');
		});

		it('Negate number', () => {
			calculator.five();
			calculator.plusmn();
			expect(calculator.outcome).toBe(-5);
		});

		it('Negate float 0.5', () => {
			calculator.comma();
			calculator.five();
			calculator.plusmn();
			expect(calculator.outcome).toBe(-0.5);
		});

		it('Remove leading zeros for -2.5', () => {
			let value = calculator.removeLeadingZeros('-2.5');
			expect(value).toBe('-2.5');
		});

		it('Negate float 2.5', () => {
			calculator.two();
			calculator.comma();
			calculator.five();
			calculator.plusmn();
			expect(calculator.outcome).toBe('-2.5');
		});
	});

	describe('Equation buttons', () => {
		it('2 squared updates calculation and outcome', () => {
			calculator.two();
			calculator.square();
			expect(calculator.outcome).toBe(4);
			expect(calculator.calculation).toBe(calculator.squareSymbol + '(2)');
		});

		it('2 squared processed correctly', () => {
			calculator.two();
			calculator.square();
			calculator.equals();
			expect(calculator.outcome).toBe(4);
			expect(calculator.calculation).toBe('');
		});

		it('2 sqr = + 5 = processed correctly', () => {
			calculator.two();
			calculator.square();
			calculator.equals();
			calculator.plus();
			calculator.five();
			calculator.equals();
			expect(calculator.outcome).toBe(9);
			expect(calculator.calculation).toBe('');
		});

		it('Calculation and outcome cleared after next number press', () => {
			calculator.two();
			calculator.square();
			calculator.two();
			expect(calculator.outcome).toBe(2);
			expect(calculator.calculation).toBe('');
		});

		it('Adds square to calculation', () => {
			calculator.two();
			calculator.plus();
			calculator.two();
			calculator.square();
			expect(calculator.outcome).toBe(4);
			expect(calculator.calculation).toBe('2 ' + calculator.plusSymbol + ' ' + calculator.squareSymbol + '(2)');
		});

		it('Last square removed after next number press', () => {
			calculator.two();
			calculator.plus();
			calculator.two();
			calculator.square();
			calculator.three();
			expect(calculator.outcome).toBe(3);
			expect(calculator.calculation).toBe('2 ' + calculator.plusSymbol + ' ');
		});

		it('Last squareroot removed after next number press', () => {
			calculator.two();
			calculator.plus();
			calculator.two();
			calculator.squareroot();
			calculator.three();
			expect(calculator.outcome).toBe(3);
			expect(calculator.calculation).toBe('2 ' + calculator.plusSymbol + ' ');
		});

		it('Last divideBy removed after next number press', () => {
			calculator.two();
			calculator.plus();
			calculator.two();
			calculator.divideBy();
			calculator.three();
			expect(calculator.outcome).toBe(3);
			expect(calculator.calculation).toBe('2 ' + calculator.plusSymbol + ' ');
		});

		it('Squareroot of 9 updates calculation and outcome', () => {
			calculator.nine();
			calculator.squareroot();
			expect(calculator.outcome).toBe(3);
			expect(calculator.calculation).toBe(calculator.squarerootSymbol + '(9)');
		});

		it('Squareroot calculation displayed correctly', () => {
			calculator.nine();
			calculator.squareroot();
			calculator.plus();
			calculator.five();
			calculator.minus();
			calculator.two();
			expect(calculator.outcome).toBe(2);
			expect(calculator.calculation).toBe(calculator.squarerootSymbol + '(9) ' + calculator.plusSymbol + ' 5 ' + calculator.minusSymbol + ' ');
		});

		it('Squareroot outcome displayed and processed correctly', () => {
			calculator.nine();
			calculator.squareroot();
			calculator.plus();
			calculator.five();
			calculator.minus();
			calculator.two();
			calculator.equals();
			expect(calculator.outcome).toBe(6);
			expect(calculator.calculation).toBe('');
		});

		it('Form equation correctly root-square', () => {
			calculator.five();
			calculator.plus();
			calculator.nine();
			calculator.square();
			calculator.squareroot();
			calculator.formEquation(this.squareSymbol);
			expect(calculator.calculation).toBe('5 + ' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(9))');
		});

		it('Nest equations correctly', () => {
			calculator.nine();
			calculator.square();
			calculator.squareroot();
			calculator.square();
			calculator.squareroot();
			expect(calculator.outcome).toBe(9);
			expect(calculator.calculation).toBe(calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(9))))');
		});

		it('Remove last equation correctly root-square', () => {
			const calculation = '5 ' + calculator.plusSymbol + ' ' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(9))';
			expect(calculator.removeLastEquationFromCalculation(calculation)).toBe('5 ' + calculator.plusSymbol);
		});

		it('Remove last equation correctly root-square-root-square', () => {
			const calculation = '5 ' + calculator.plusSymbol + ' ' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(9))))';
			expect(calculator.removeLastEquationFromCalculation(calculation)).toBe('5 ' + calculator.plusSymbol);
		});

		it('Nest equations correctly after a calculation', () => {
			calculator.five();
			calculator.plus();
			calculator.five();
			calculator.plus();
			calculator.nine();
			calculator.square();
			calculator.squareroot();
			calculator.square();
			calculator.squareroot();
			expect(calculator.outcome).toBe(9);
			expect(calculator.calculation).toBe('5 ' + calculator.plusSymbol + ' 5 ' + calculator.plusSymbol + ' ' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(9))))');
		});

		it('Minus equation processed correctly', () => {
			calculator.minus();
			calculator.five();
			calculator.square();
			calculator.equals();
			expect(calculator.outcome).toBe(-25);
		});

		it('Equation times number processed correctly', () => {
			calculator.six();
			calculator.plus();
			calculator.five();
			calculator.square();
			calculator.squareroot();
			calculator.divideBy();
			calculator.times();
			calculator.five();
			calculator.equals();
			calculator.plus();
			expect(calculator.calculation).toBe('7 ' + calculator.plusSymbol + ' ');
			expect(calculator.outcome).toBe(7);
		});

		it('Divide by then square processed correctly', () => {
			calculator.five();
			calculator.divideBy();
			calculator.square();
			expect(calculator.calculation).toBe(calculator.squareSymbol + '(' + calculator.divideBySymbol + '(5))');
			// expect(calculator.outcome).toBe('0.04'); // 0.04000000000000001
		});

		it('X Minus, Squareroot, Square, DivideBy Y displayed correctly', () => {
			calculator.five();
			calculator.minus();
			calculator.two();
			calculator.divideBy();
			calculator.square();
			calculator.squareroot();
			expect(calculator.error).toBe(false);
			expect(calculator.calculation).toBe('5 ' + calculator.minusSymbol + ' ' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(' + calculator.divideBySymbol + '(2)))');
			expect(calculator.outcome).toBe('0.5');
		});
	});

	describe('Percentage button', () => {
		it('20 percent added to 100 updates calculation and outcome', () => {
			calculator.one();
			calculator.zero();
			calculator.zero();
			calculator.plus();
			calculator.two();
			calculator.zero();
			calculator.percent();
			expect(calculator.outcome).toBe(20);
			expect(calculator.calculation).toBe('100 ' + calculator.plusSymbol + ' 20');
		});

		it('20 percent added to 100 processed correctly', () => {
			calculator.one();
			calculator.zero();
			calculator.zero();
			calculator.plus();
			calculator.two();
			calculator.zero();
			calculator.percent();
			calculator.equals();
			expect(calculator.outcome).toBe(120);
			expect(calculator.calculation).toBe('');
		});

		it('Calculation and outcome cleared after next number press', () => {
			calculator.one();
			calculator.zero();
			calculator.zero();
			calculator.plus();
			calculator.two();
			calculator.zero();
			calculator.percent();
			calculator.five();
			expect(calculator.outcome).toBe(5);
			expect(calculator.calculation).toBe('100 ' + calculator.plusSymbol + ' ');
		});

		it('Calculates percentage based on old calculation', () => {
			calculator.one();
			calculator.zero();
			calculator.zero();
			calculator.plus();
			calculator.five();
			calculator.zero();
			calculator.plus();
			calculator.five();
			calculator.zero();
			calculator.percent();
			expect(calculator.outcome).toBe(75);
			expect(calculator.calculation).toBe('100 ' + calculator.plusSymbol + ' 50 ' + calculator.plusSymbol + ' 75');
		});

		it('Last percentage removed after next number press', () => {
			calculator.one();
			calculator.zero();
			calculator.zero();
			calculator.plus();
			calculator.five();
			calculator.zero();
			calculator.plus();
			calculator.five();
			calculator.zero();
			calculator.percent();
			calculator.five();
			expect(calculator.outcome).toBe(5);
			expect(calculator.calculation).toBe('100 ' + calculator.plusSymbol + ' 50 ' + calculator.plusSymbol + ' ');
		});

		it('Correctly determine percent from equation', () => {
			calculator.five();
			calculator.plus();
			calculator.five();
			calculator.square();
			calculator.squareroot();
			calculator.percent();
			expect(calculator.outcome).toBe('0.25');
			expect(calculator.calculation).toBe('5 ' + calculator.plusSymbol + ' 0.25');
		});
	});

	describe('Percentages and equations', () => {
		it('Correctly apply equation to percentage', () => {
			calculator.four();
			calculator.plus();
			calculator.five();
			calculator.percent();
			calculator.square();
			// expect(calculator.outcome).toBe('0.04'); 0.04000000000000001
			expect(calculator.calculation).toBe('4 ' + calculator.plusSymbol + ' ' + calculator.squareSymbol + '(0.2)');
		});

		it('Clear after square and percentage', () => {
			calculator.three();
			calculator.square();
			calculator.percent();
			expect(calculator.calculation).toBe('0');
			expect(calculator.outcome).toBe(0);
		});

		it('Clear after squareroot and percentage', () => {
			calculator.three();
			calculator.squareroot();
			calculator.percent();
			expect(calculator.calculation).toBe('0');
			expect(calculator.outcome).toBe(0);
		});

		it('Clear after divideBy and percentage', () => {
			calculator.three();
			calculator.divideBy();
			calculator.percent();
			expect(calculator.calculation).toBe('0');
			expect(calculator.outcome).toBe(0);
		});
	});

	describe('Divide by zero', () => {
		it('Cannot divide 1 by zero', () => {
			calculator.divideBy();
			expect(calculator.calculation).toBe('1/(0)');
			expect(calculator.error).toBe('Cannot divide by zero');
		});

		it('Cannot divide 5 by zero', () => {
			calculator.five();
			calculator.divide();
			calculator.zero();
			calculator.equals();
			expect(calculator.calculation).toBe('5 ' + calculator.divideSymbol + ' ');
			expect(calculator.error).toBe('Cannot divide by zero');
		});

		it('Cannot divide equation by zero', () => {
			calculator.square();
			calculator.squareroot();
			calculator.divideBy();
			expect(calculator.calculation).toBe(calculator.divideBySymbol + '(' + calculator.squarerootSymbol + '(' + calculator.squareSymbol + '(0)))');
			expect(calculator.error).toBe(calculator.cannotDivideByZero);
		});

		it('Cannot divide 5 by equation equal to zero', () => {
			calculator.five();
			calculator.divide();
			calculator.zero();
			calculator.square();
			calculator.equals();
			expect(calculator.calculation).toBe('5 ' + calculator.divideSymbol + ' ' + calculator.squareSymbol + '(0)');
			expect(calculator.error).toBe('Cannot divide by zero');
		});
	});

	describe('Comma button', () => {
		it('Correctly add comma numbers to calculation', () => {
			calculator.six();
			calculator.plus();
			calculator.comma();
			calculator.five();
			calculator.equals();
			expect(calculator.outcome).toBe('6.5');
		});

		it('Equals should remove comma', () => {
			calculator.two();
			calculator.comma();
			calculator.equals();
			expect(calculator.outcome).toBe(2);
		});

		it('Equals should reset outcome', () => {
			calculator.two();
			calculator.equals();
			calculator.comma();
			expect(calculator.outcome).toBe('0.');
		});

		it('Equals should reset outcome (part 2)', () => {
			calculator.two();
			calculator.equals();
			calculator.comma();
			calculator.two();
			calculator.plus();
			calculator.two();
			expect(calculator.calculation).toBe('0.2 ' + calculator.plusSymbol + ' ');
			expect(calculator.outcome).toBe(2);
		});
	});

	describe('Backspace button', () => {
		it('should leave outcome to zero if outcome empty', () => {
			calculator.backspace();
			expect(calculator.outcome).toBe(0);
			expect(calculator.calculation).toBe('');
		});

		it('should leave outcome to zero if outcome empty', () => {
			calculator.one();
			calculator.two();
			calculator.backspace();
			calculator.backspace();
			calculator.backspace();
			expect(calculator.outcome).toBe(0);
			expect(calculator.calculation).toBe('');
		});

		it('should remove last character from outcome', () => {
			calculator.one();
			calculator.two();
			calculator.three();
			calculator.backspace();
			expect(calculator.outcome).toBe(12);
		});

		it('should not impact outcome after equals', () => {
			calculator.one();
			calculator.plus();
			calculator.two();
			calculator.equals();
			calculator.backspace();
			expect(calculator.outcome).toBe(3);
		});

		it('should not impact outcome after equationpress', () => {
			calculator.two();
			calculator.square();
			calculator.backspace();
			expect(calculator.outcome).toBe(4);
		});

		it('should not impact outcome after percent', () => {
			calculator.one();
			calculator.zero();
			calculator.zero();
			calculator.plus();
			calculator.five();
			calculator.zero();
			calculator.percent();
			calculator.backspace();
			expect(calculator.outcome).toBe(50);
		});
	});

	describe('Errors', () => {
		it('should clear after backspace press', () => {
			calculator.divideBy();
			calculator.equals();
			calculator.backspace();
			expect(calculator.outcome).toBe(0);
			expect(calculator.calculation).toBe('');
		});

		it('should be cleared after equals press', () => {
			calculator.divideBy();
			calculator.equals();
			expect(calculator.outcome).toBe(0);
			expect(calculator.calculation).toBe('');
		});

		it('should be cleared after number press and number should be added', () => {
			calculator.divideBy();
			calculator.one();
			expect(calculator.outcome).toBe(1);
			expect(calculator.calculation).toBe('');
		});
	})
});