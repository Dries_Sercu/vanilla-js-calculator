#Read me

This project is a copy of the windows calculator functionality.

It is meant to be an exercise in vanilla javascript. That means that if anyone has any feedback regarding improvements or questions, feel free to contact me.

## npm

Npm is only required to execute tests and review the code coverage.

`npm install`

`npm install -g jasmine`

### Tests

Tests can be run using `jasmine` in the console.

### Coverage

To get the coverage reports run `npm run test-cover`.
To view the coverage reports go to `coverage/lcov-report/vanilla-js-calculator/index.html`

## Known issues

### Javascript eval()

`eval("21.6 + 0.8") = 22.400000000000002`

Read more about the issue here:
https://stackoverflow.com/questions/1458633/how-to-deal-with-floating-point-number-precision-in-javascript

I will not fix this issue as it is not within the scope of the intended exercise. 

### Bug in the windows calculator

`100 + 10% + 25 * 1%`   
= 143.75 in JS eval()

= 182,25 in WINDOWS

10% of 100 = 10
1% of 135 = 1.35 (both calculators do this the right way)

meaning the calculation becomes:
`100 + 10 + 25 * 1.35`

\* takes precedence over  +, so:

`(100 + 10) + (25 * 1.35)` is correct

and not: `(100 + 10 + 25) * 1.35`