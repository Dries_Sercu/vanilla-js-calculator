class Calculator {
	constructor(element) {
		// the symbols are displayed in calculation
		this.plusSymbol = '+';
		this.minusSymbol = '-';
		this.divideBySymbol = '1/';
		this.divideSymbol = '&divide;';
		this.timesSymbol = '&times;';
		this.squareSymbol = 'sqr';
		this.squarerootSymbol = '&radic;';

		this.cannotDivideByZero = 'Cannot divide by zero';
		// symbolMap is used to translate calculation before eval can process
		this.symbolMap = {};
		this.symbolMap[this.divideSymbol] = '/';
		this.symbolMap[this.timesSymbol] = '*';
		this.symbolMap[this.squareSymbol] = 'this.processSquareCalculation';
		this.symbolMap[this.squarerootSymbol] = 'this.processSquareRootCalculation';

		this.initializeDOM(element);
		this.error = false;
		this.clear();
	}

	initializeDOM(element) {
		if (!element) return;
		this.DOM = {el: element};

		this.DOM.percentBtn = this.DOM.el.querySelector('button.percent');
		this.DOM.squarerootBtn = this.DOM.el.querySelector('button.squareroot');
		this.DOM.squareBtn = this.DOM.el.querySelector('button.square');
		this.DOM.divideByBtn = this.DOM.el.querySelector('button.divide-by');
		this.DOM.cancelEntryBtn = this.DOM.el.querySelector('button.cancel-entry');
		this.DOM.clearBtn = this.DOM.el.querySelector('button.clear');
		this.DOM.backspaceBtn = this.DOM.el.querySelector('button.backspace');
		this.DOM.divideBtn = this.DOM.el.querySelector('button.divide');
		this.DOM.sevenBtn = this.DOM.el.querySelector('button.seven');
		this.DOM.eightBtn = this.DOM.el.querySelector('button.eight');
		this.DOM.nineBtn = this.DOM.el.querySelector('button.nine');
		this.DOM.timesBtn = this.DOM.el.querySelector('button.times');
		this.DOM.fourBtn = this.DOM.el.querySelector('button.four');
		this.DOM.fiveBtn = this.DOM.el.querySelector('button.five');
		this.DOM.sixBtn = this.DOM.el.querySelector('button.six');
		this.DOM.minusBtn = this.DOM.el.querySelector('button.minus');
		this.DOM.oneBtn = this.DOM.el.querySelector('button.one');
		this.DOM.twoBtn = this.DOM.el.querySelector('button.two');
		this.DOM.threeBtn = this.DOM.el.querySelector('button.three');
		this.DOM.plusBtn = this.DOM.el.querySelector('button.plus');
		this.DOM.plusmnBtn = this.DOM.el.querySelector('button.plusmn');
		this.DOM.zeroBtn = this.DOM.el.querySelector('button.zero');
		this.DOM.commaBtn = this.DOM.el.querySelector('button.comma');
		this.DOM.equalsBtn = this.DOM.el.querySelector('button.equals');

		this.DOM.calculation = this.DOM.el.querySelector('.calculation');
		this.DOM.outcome = this.DOM.el.querySelector('.outcome');

		this.addEventListeners();
	}

	addEventListeners() {
		this.DOM.percentBtn.addEventListener('click', () => this.percent());
		this.DOM.squarerootBtn.addEventListener('click', () => this.squareroot());
		this.DOM.squareBtn.addEventListener('click', () => this.square());
		this.DOM.divideByBtn.addEventListener('click', () => this.divideBy());
		this.DOM.cancelEntryBtn.addEventListener('click', () => this.cancelEntry());
		this.DOM.clearBtn.addEventListener('click', () => this.clear());
		this.DOM.backspaceBtn.addEventListener('click', () => this.backspace());
		this.DOM.divideBtn.addEventListener('click', () => this.divide());
		this.DOM.sevenBtn.addEventListener('click', () => this.seven());
		this.DOM.eightBtn.addEventListener('click', () => this.eight());
		this.DOM.nineBtn.addEventListener('click', () => this.nine());
		this.DOM.timesBtn.addEventListener('click', () => this.times());
		this.DOM.fourBtn.addEventListener('click', () => this.four());
		this.DOM.fiveBtn.addEventListener('click', () => this.five());
		this.DOM.sixBtn.addEventListener('click', () => this.six());
		this.DOM.minusBtn.addEventListener('click', () => this.minus());
		this.DOM.oneBtn.addEventListener('click', () => this.one());
		this.DOM.twoBtn.addEventListener('click', () => this.two());
		this.DOM.threeBtn.addEventListener('click', () => this.three());
		this.DOM.plusBtn.addEventListener('click', () => this.plus());
		this.DOM.plusmnBtn.addEventListener('click', () => this.plusmn());
		this.DOM.zeroBtn.addEventListener('click', () => this.zero());
		this.DOM.commaBtn.addEventListener('click', () => this.comma());
		this.DOM.equalsBtn.addEventListener('click', () => this.equals());

		// bind this to addKeyboard method to provide the context
		document.addEventListener('keydown', (event) => normalizeKeyboardEvent(event, this.addKeyboard.bind(this)));
	}

	addKeyboard(code) {
		switch (code) {
		case '0':
			this.clickBtnStyle(this.DOM.zeroBtn);
			this.zero();
			break;
		case '1':
			this.clickBtnStyle(this.DOM.oneBtn);
			this.one();
			break;
		case '2':
			this.clickBtnStyle(this.DOM.twoBtn);
			this.two();
			break;
		case '3':
			this.clickBtnStyle(this.DOM.threeBtn);
			this.three();
			break;
		case '4':
			this.clickBtnStyle(this.DOM.fourBtn);
			this.four();
			break;
		case '5':
			this.clickBtnStyle(this.DOM.fiveBtn);
			this.five();
			break;
		case '6':
			this.clickBtnStyle(this.DOM.sixBtn);
			this.six();
			break;
		case '7':
			this.clickBtnStyle(this.DOM.sevenBtn);
			this.seven();
			break;
		case '8':
			this.clickBtnStyle(this.DOM.eightBtn);
			this.eight();
			break;
		case '9':
			this.clickBtnStyle(this.DOM.nineBtn);
			this.nine();
			break;
		case '.':
		case ',':
			this.clickBtnStyle(this.DOM.commaBtn);
			this.comma();
			break;
		case '/':
			this.clickBtnStyle(this.DOM.divideBtn);
			this.divide();
			break;
		case '*':
			this.clickBtnStyle(this.DOM.timesBtn);
			this.times();
			break;
		case '-':
			this.clickBtnStyle(this.DOM.minusBtn);
			this.minus();
			break;
		case '+':
			this.clickBtnStyle(this.DOM.plusBtn);
			this.plus();
			break;
		case '%':
			this.clickBtnStyle(this.DOM.percentBtn);
			this.percent();
			break;
		case 'Escape':
			this.clickBtnStyle(this.DOM.clearBtn);
			this.clear();
			break;
		case 'Delete':
			this.clickBtnStyle(this.DOM.cancelEntryBtn);
			this.cancelEntry();
			break;
		case 'Backspace':
			this.clickBtnStyle(this.DOM.backspaceBtn);
			this.backspace();
			break;
		case 'Enter':
			this.clickBtnStyle(this.DOM.equalsBtn);
			this.equals();
			break;
		default:
		}
	}

	clickBtnStyle(btn) {
		btn.classList.add('click');
		setTimeout(() => {
			btn.classList.remove('click');
		}, 100);
	}

	squareroot() {
		this.equationPress(this.formEquation(this.squarerootSymbol));
	}

	percent() {
		if (this.calculation.length < 1 || this.outcomeAlreadyAddedToCalculation) return;

		let tempCalculation = this.calculation;
		// process current calculation (without last symbol) = tempOutcome
		if (!this.calculationEndsInSymbol(tempCalculation)) {
			tempCalculation = this.removeLastEquationFromCalculation(tempCalculation);
			this.calculation = (tempCalculation) ? tempCalculation + ' ' : '';
		}
		if (this.calculationEndsInSymbol(tempCalculation)) {
			tempCalculation = this.removeLastSymbolFromCalculation(tempCalculation);
		}

		const tempOutcome = this.processCalculation(tempCalculation);

		// calculate current outcome percentage of tempOutcome = percentage
		const percentage = tempOutcome * this.outcome / 100;
		this.calculation += percentage;
		this.outcome = percentage;

		this.enableBackspace = false;
		this.outcomeAlreadyAddedToCalculation = true;

		this.updateCalculation();
		this.updateOutcome();
	}

	square() {
		this.equationPress(this.formEquation(this.squareSymbol));
	}

	divideBy() {
		this.equationPress(this.formEquation(this.divideBySymbol));
	}

	// if last calculation has no symbol, apply equation to calculation 1/(1/(x)) equation(equation(x))
	formEquation(equation) {
		if (this.calculationEndsInEquation(this.calculation)) {
			return equation + '(' + this.getLastEquation(this.calculation) + ')';
		} else {
			return equation + '(' + this.outcome + ')';
		}
	}

	cancelEntry() {
		if (this.error) {
			this.error = false;
			this.enableButtonsForError();
			this.clear();
			return;
		}
		this.outcome = 0;
		this.addComma = false;
		this.resetOutcome = false;
		this.enableBackspace = true;
		this.lastCalculation = null;
		this.outcomeIsEquationResult = false;
		this.outcomeAlreadyAddedToCalculation = false;
		this.updateOutcome();
	}

	clear() {
		this.cancelEntry();
		this.calculation = '';
		this.updateCalculation();
	}

	backspace() {
		if (!this.enableBackspace) {
			return;
		}
		if (this.error || !isFinite(this.outcome)) {
			this.cancelEntry();
			return;
		}
		this.outcome = this.outcome.toString().slice(0, -1);
		this.updateOutcome();
	}

	divide() {
		this.symbolPress(this.divideSymbol);
	}

	seven() {
		this.addToOutcome(7);
	}

	eight() {
		this.addToOutcome(8);
	}

	nine() {
		this.addToOutcome(9);
	}

	times() {
		this.symbolPress(this.timesSymbol);
	}

	four() {
		this.addToOutcome(4);
	}

	five() {
		this.addToOutcome(5);
	}

	six() {
		this.addToOutcome(6);
	}

	minus() {
		this.symbolPress(this.minusSymbol);
	}

	one() {
		this.addToOutcome(1);
	}

	two() {
		this.addToOutcome(2);
	}

	three() {
		this.addToOutcome(3);
	}

	plus() {
		this.symbolPress(this.plusSymbol);
	}

	plusmn() {
		this.outcome = -this.outcome;
		this.updateOutcome();
	}

	zero() {
		this.addToOutcome(0);
	}

	comma() {
		this.addComma = true;
		this.updateOutcome();
	}

	equals() {
		if (this.error) {
			this.clear();
			return;
		}
		if (this.lastCalculation === null) {
			this.setLastCalculation();
		}
		try {
			let fullCalculation = '';
			if (this.calculation.length > 0) {
				if (this.lastCalculation) {
					fullCalculation = this.removeLastSymbolFromCalculation(this.calculation);
					if (this.outcomeIsEquationResult) {
						fullCalculation = this.removeLastSymbolFromCalculation(this.removeLastEquationFromCalculation(this.calculation));
					}
				} else {
					fullCalculation = this.calculation;
				}
			} else {
				fullCalculation = this.outcome;
			}
			if (this.lastCalculation && !this.outcomeAlreadyAddedToCalculation) {
				fullCalculation += this.lastCalculation;
			}
			this.outcome = this.processCalculation(fullCalculation);
			this.resetOutcome = true;
			this.outcomeIsEquationResult = false;
			this.outcomeAlreadyAddedToCalculation = false;
			this.enableBackspace = false;
			this.calculation = '';
			this.updateCalculation();
			this.updateOutcome();
		} catch (e) {
			this.handleError(e);
		}
	}

	symbolPress(symbol) {
		if (this.resetOutcome && this.calculationEndsInSymbol(this.calculation)) {
			// remove symbol at the end of calculation
			this.calculation = this.removeLastSymbolFromCalculation(this.calculation);
			this.addSymbolToCalculation(symbol);
			this.updateCalculation();
			return;
		}
		if (this.calculationEndsInEquation(this.calculation)) {
			// triggered when adding symbol after equation or percent
			this.addSymbolToCalculation(symbol);
			this.updateCalculation();
			return;
		}
		this.appendOutcomeToCalculation();
		try {
			this.outcome = this.processCalculation(this.calculation);
			this.addSymbolToCalculation(symbol);
			this.updateCalculation();
			this.updateOutcome();
		} catch (e) {
			this.handleError(e);
		}
	}

	equationPress(equation) {
		if (this.calculationEndsInEquation(this.calculation)) {
			this.calculation = this.removeLastEquationFromCalculation(this.calculation);
			if (this.calculation.length > 0) {
				this.calculation += ' ';
			}
		}
		this.calculation = this.removeLastNumberFromCalculation(this.calculation);
		this.calculation += equation;
		try {
			this.updateCalculation();
			this.outcome = this.processCalculation(equation);
			this.outcomeIsEquationResult = true;
			this.updateOutcome();
			this.enableBackspace = false;
		} catch (e) {
			this.handleError(e);
		}
	}

	setLastCalculation() {
		const lastSymbol = this.getLastSymbol(this.calculation);
		if (!lastSymbol) return;
		let lastNumber;
		if (this.outcomeIsEquationResult) {
			lastNumber = this.getLastEquation(this.calculation);
		} else {
			lastNumber = this.outcome;
		}
		this.lastCalculation = (lastSymbol + ' ' + lastNumber).trim();
	}

	// Adds number to outcome
	addToOutcome(value) {
		if (this.error) {
			this.clear();
		}
		if (this.resetOutcome) {
			this.outcome = '';
			this.resetOutcome = false;
		}

		if ((this.outcomeIsEquationResult || this.outcomeAlreadyAddedToCalculation)) {
			this.calculation = this.removeLastNumberFromCalculation(this.calculation);
			this.updateCalculation();
		}

		this.outcomeIsEquationResult = false;
		this.outcomeAlreadyAddedToCalculation = false;
		this.enableBackspace = true;
		this.outcome = this.outcome.toString() + value.toString();
		this.updateOutcome();
	}

	// updates the outcome html
	updateOutcome() {
		if (this.DOM && this.error) {
			this.DOM.outcome.innerHTML = this.error;
			return;
		}

		if (this.outcome.length === 0) {
			this.outcome = 0;
		}

		this.outcome = this.removeLeadingZeros(this.outcome);

		if (this.addComma && this.outcome.toString().indexOf('.') < 0) {
			if (this.resetOutcome) {
				this.outcome = '0';
				this.resetOutcome = false;
			}
			this.outcome += '.';
			this.addComma = false;
		}

		if (!this.DOM) return;
		this.DOM.outcome.innerHTML = this.numberWithSpaces(this.outcome);
	}

	// updates the calculation html
	updateCalculation() {
		if (!this.DOM) return;
		if (this.calculation.length === 0) {
			this.DOM.calculation.innerHTML = '&nbsp;';
		} else {
			this.DOM.calculation.innerHTML = this.calculation;
		}
	}

	// https://stackoverflow.com/questions/16637051/adding-space-between-numbers
	numberWithSpaces(x) {
		const parts = x.toString().split('.');
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
		return parts.join('.');
	}

	// removes last number / equation from calculation
	removeLastNumberFromCalculation(calculation) {
		if (this.calculationEndsInSymbol(calculation)) return calculation;
		const calculationArr = calculation.trim().split(' ');
		calculationArr.pop();
		calculation = calculationArr.join(' ');

		if (calculation.length > 0) {
			calculation += ' ';
		}

		return calculation;
	}

	removeLastSymbolFromCalculation(calculation) {
		if (this.calculationEndsInSymbol(calculation)) {
			const calculationArr = calculation.trim().split(' ');
			calculationArr.pop();
			return calculationArr.join(' ');
		}
		return this.removeLastEquationFromCalculation(calculation);
	}

	removeLastEquationFromCalculation(calculation) {
		if (!this.calculationEndsInEquation(calculation)) {
			return calculation;
		}

		const calculationArr = calculation.split(' ');
		for (let i = calculationArr.length - 1; i > -1; i--) {
			if (this.calculationEndsInSymbol(calculationArr[i])) {
				break;
			}
			calculationArr.splice(i, 1);
		}
		return calculationArr.join(' ');
	}

	addSymbolToCalculation(symbol) {
		this.lastCalculation = null;
		if (this.calculationEndsInSymbol(this.calculation)) {
			this.calculation.replace(this.getLastSymbol(), ' ' + symbol + ' ');
		} else {
			this.calculation += ' ' + symbol + ' ';
		}
	}

	getLastSymbol(calculation) {
		// regex: whitespace, words, digits, () . (commas and equations become "this.___")and '/(' for divideBy
		const symbols = this.translateCalculation(calculation).replace(/[\s\w\d().](\/\()?/g, '');
		if (symbols.length < 1) return '';
		return symbols[symbols.length - 1];
	}

	getLastEquation(calculation) {
		if (this.calculationEndsInSymbol(calculation)) {
			return '';
		}
		const calculationArr = calculation.split(' ');
		return calculationArr[calculationArr.length - 1];
	}

	// + - &times; &divide;
	calculationEndsInSymbol(calculation) {
		const characters = calculation.trim();
		if (characters.length < 1 || this.calculationEndsInEquation(calculation)) return false;
		return Number.isNaN(parseFloat(characters[characters.length - 1]));
	}

	// divide by '1/(x)', square 'sqr(x)'
	calculationEndsInEquation(calculation) {
		calculation = calculation.trim();
		return (calculation[calculation.length - 1] === ')');
	}

	appendOutcomeToCalculation() {
		if (!this.outcomeIsEquationResult) {
			this.calculation = this.calculation.toString() + this.outcome.toString();
		}
	}

	processCalculation(calculation) {
		// clear outcome on next number press
		this.resetOutcome = true;
		calculation = calculation.toString();
		if (calculation === '') return 0;

		if (!this.calculationEndsInSymbol(calculation)) {
			const translatedCalculation = this.translateCalculation(calculation);
			const divideByZero = this.hasDivideByZero(translatedCalculation);
			if (divideByZero) {
				throw this.cannotDivideByZero;
			}
			return eval(translatedCalculation);
		}
		return calculation;
	}

	hasDivideByZero(calculation) {
		// don't capture but start with "/"
		// capture as little of anything possible until
		// don't capture whitespace or end of string
		const regex = new RegExp('(?:\\/)(.+?)(?:\\))*(?:\\s|$)', 'g');
		const matches = [];
		let match;
		while (match = regex.exec(calculation)) {
			matches.push(match[1]);
		}
		if (matches.length < 1) return false;
		return matches.some((value) => {
			const openParentheses = value.match(/\(/g) || [];
			const closingParentheses = value.match(/\)/g) || [];
			for (let i = closingParentheses.length; i < openParentheses.length; i++) {
				value += ')';
			}
			return this.processCalculation(value) === 0;
		});
	}

	// translates html special characters to eval-able symbols
	translateCalculation(calculation) {
		Object.keys(this.symbolMap).forEach((key) => {
			const regex = new RegExp(key, 'g');
			calculation = calculation.replace(regex, this.symbolMap[key]);
		});
		return calculation;
	}

	processSquareCalculation(val) {
		return Math.pow(val, 2);
	}

	processSquareRootCalculation(val) {
		return Math.sqrt(val);
	}

	removeLeadingZeros(value) {
		const arr = value.toString().split('.');
		if (arr[1] && arr[1] !== '') {
			const negative = (value < 0);
			value = parseInt(arr[0].toString()) + '.' + arr[1];
			// parseInt removes minus from "-0" in for example "-0.5"
			if (negative && value > 0) value = -value;
			return value;
		}
		return parseInt(arr[0].toString());
	}

	handleError(message) {
		this.error = message;
		this.updateOutcome();
		this.disableButtonsForError();
	}

	disableButtonsForError() {
		if (!this.DOM) return;
		this.DOM.percentBtn.disabled = true;
		this.DOM.squarerootBtn.disabled = true;
		this.DOM.squareBtn.disabled = true;
		this.DOM.divideByBtn.disabled = true;
		this.DOM.divideBtn.disabled = true;
		this.DOM.timesBtn.disabled = true;
		this.DOM.minusBtn.disabled = true;
		this.DOM.plusBtn.disabled = true;
		this.DOM.plusmnBtn.disabled = true;
		this.DOM.commaBtn.disabled = true;
	}

	enableButtonsForError() {
		if (!this.DOM) return;
		this.DOM.percentBtn.disabled = false;
		this.DOM.squarerootBtn.disabled = false;
		this.DOM.squareBtn.disabled = false;
		this.DOM.divideByBtn.disabled = false;
		this.DOM.divideBtn.disabled = false;
		this.DOM.timesBtn.disabled = false;
		this.DOM.minusBtn.disabled = false;
		this.DOM.plusBtn.disabled = false;
		this.DOM.plusmnBtn.disabled = false;
		this.DOM.commaBtn.disabled = false;
	}
}


function normalizeKeyboardEvent(event, callback) {
	let code;
	if (event.key !== undefined) {
		code = event.key;
	} else if (event.keyIdentifier !== undefined) {
		code = event.keyIdentifier;
	} else if (event.keyCode !== undefined) {
		code = event.keyCode;
	}
	callback(code);
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = Calculator;
}
